package com.thanakit.week5;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void shouldAdd1And1IS2(){
        int result = App.add(1 , 1);
        assertEquals(2, result);
    }
    @Test
    public void shouldAdd1And2IS3(){
        int result = App.add(1 , 2);
        assertEquals(3, result);
    }
    @Test
    public void shouldAddMin1And0ISmin1(){
        int result = App.add(-1 , 0);
        assertEquals(-1, result);
    }
}
