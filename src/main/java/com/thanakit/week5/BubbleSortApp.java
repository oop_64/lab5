package com.thanakit.week5;

public class BubbleSortApp {
    public static void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

    public static void buble(int[] arr, int first, int second) {
        for(int i = first; i <second; i++){
            if(arr[i]>arr[i+1]){
                swap(arr, i, i+1);
            }
        }
    }

    public static void bubleSort(int[] arr) {
        for(int i = arr.length-1; i>0; i--){
            buble(arr,0 ,i);
        }
    }
}
